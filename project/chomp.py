from gasp import *

class Maze:
    def __init__(self):
        self.is_done = False
        
    def finished(self):
        return self.is_done

    def play(self):
        answered = input("are we done yet?")
        if answered == "y":
            self.is_done = True
        else:
            print("I'm playing")

    def done(self):
        print("I'm done")

    
the_maze = Maze()

while not the_maze.finished():
    the_maze.play()

the_maze.done()
