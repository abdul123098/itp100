import pygame

from pygame.locals import *
from random import randint
import os, sys

ARRAY_SIZE = 50

DIRECTIONS = {
    "LEFT": (-1, 0),
    "RIGHT": (1, 0),
    "UP": (0, 1),
    "DOWN": (0, -1),
}

snake, fruit = None, None

def init():
    global snake
    snake = [ (0, 2), (0, 1), (0, 0)]

    place_fruit((ARRAY_SIZE // 2, ARRAY_SIZE // 2))

def place_fruit(r=None):
    global fruit
    if r:
        fruit = r
        return


