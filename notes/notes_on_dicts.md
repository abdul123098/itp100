1. Describe the characteristics of a collection. Collections in Python are containers that are used to store collections of data, for example, list, dict, set, tuple etc. These are built-in collections. 

2. Using the video for inspiration, make a high level comparision of lists and dictionaries. How should you think about each of them?     A list is an ordered sequence of objects, whereas dictionaries are unordered sets. However, the main difference is that items in dictionaries are accessed via keys and not via their position

 3.  What is the synonym Dr. Chuck tells us for dictionaries, that he presents in the slide showing a similiar feature in other programming languages?
Associate array - Perl / PHP, Properties or map or Hashmap - java, property bag -C# / .Net

4. Show a few examples of dictionary literals being assigned to variables. >>> eng2sp = {'one': 'uno', 'two': 'dos', 'three': 'tres'} >>> print(eng2sp) {'one': 'uno', 'three': 'tres', 'two': 'dos'}

5. Describe the application in the second video which Dr. Chuck says is one of the most common uses of dictionaries.   Used to store data values like a map, which unlike other Data Types that hold only single value as an element, Dictionary holds key:value pair. Key value is provided in the dictionary to make it more optimized.

6. Write down the code Dr. Chuck presents run this application.   Counts = dict() names = ['csev','cwen','csev','zqian','cwen'] for name in names:  if name not in counts: counts[name] = 1 else: counts[name] = counts[name]+1 print(counts)

7. What is the dictionary method that makes this common operation shorter (from 4 lines to 1)? Describe how to use it.   The pattern of checking to see if a key is already in a dictionary and assuming a default value if the key is not there is so common that there is a method called get() that does this for us. 

8. Describe the application that Dr. Chuck leads into in the third video and codes in front of us in the fourth video.   

