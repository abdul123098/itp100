What is a library? it is a collection of non volatile resources and it is used by computer programs. These may include message templates, classes, documentation. When you use a library, program gains the behavior implemented inside that library without having to implement that behavior itself.
https://www.webopedia.com/definitions/library/. https://www.computerhope.com/jargon/l/library.htm.  https://blog.alexdevero.com/programming-languages-libraries-and-frameworks/ 


What is framework? it is a platform for developing software applications. It gives a foundation on which a software developer can build programs. These may also include a compiler, code libraries. Some examples of the most popular frameworks today can be for all-in-one web development (HTML, CSS and JavaScript) Twitter’s Bootstrap, ZURB’s Foundation, Front-end Boilerplate or Themosis.
https://en.wikipedia.org/wiki/Software_framework.  https://blog.alexdevero.com/programming-languages-libraries-and-frameworks/.    https://www.geeksforgeeks.org/software-framework-vs-library/
