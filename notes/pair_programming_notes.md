1. Why pair program? What benefits do the videos claim for the practice?
   It is efficient and effective way to share knowledge with others. It saves      time by making fewer mistakes in coding. It also makes it easier to ask for     help when you are stuck in coding.       

2. What are the two distinct roles used in the practice of pair programming? How   does having only one computer for two programmers aide in the pair adopting     these roles?
   One person writes the code and the other person help code what to write next.   It helps to be more sociable and solve problems in coding.   
