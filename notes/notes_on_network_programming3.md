1. What is ASCII? What are its limitations and why do we need to move beyond it? ASCII is based on an older teletype character encoding scheme which itself was based on an even older telegraph character encoding scheme. Someone might say that 7-bit ASCII main limitation is that it only encoded 128 characters, 32 of which are control characters.

2. Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)? Ord (‘H’)

3. Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?UTF-8

4. Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.It can convert objects into bytes objects, or create empty bytes object of the specified size. The difference between bytes() and bytearray() is that bytes() returns an object that cannot be modified, and bytearray() returns an object that can be modified.

5. Break down the process of using .encode() and .decode methods on data we send over a network connection. Encoding is the process of putting a sequence of characters (letters, numbers, punctuation, and certain symbols) into a specialized format for efficient transmission or storage. Decoding is the opposite process -- the conversion of an encoded format back into the original sequence of characters. 

