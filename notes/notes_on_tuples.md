1. Dr. Chuck begins this lesson by stating that tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike? They have parenthesis instead of square bracket and they position like 0,1,2 like a list.

2. How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?  list are mutable    while tuples are immutable. List has variable size while tuple has a fixed size.With tuples, you cant change anything. 

3. r. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?   Since Python does not have to build tuple structures to be modifiable, they are simpler and more efficient in terms of memory use and performance than lists. >>> (a,b) = (5, 99)  >>> print(a)  5  

4.Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.  I learned that tuples are immutable, it can not be changed once it's written. Tuples are written with round brackets. >>> t = ('a', 'b', 'c', 'd', 'e')
>>> print(t[0])
'a'     >>> print(t[1:3])
('b', 'c')
>>> (0, 1, 2) < (0, 3, 4)
True

5. In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?  yes
