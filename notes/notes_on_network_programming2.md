1. what is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?   defines how application processes (clients and servers), running on different end systems, pass messages to each other.

2. Name the three parts of a URL. What does each part specify? protocol, the domain name and the path 

3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works? traces how a user's request flows through the app. Understanding the request/response cycle is helpful to figure out which files to edit when developing an app

4.What is the IETF? What is an RFC? It is a publication from the Internet Society (ISOC) and its associated bodies, most prominently the Internet Engineering Task Force (IETF), the principal technical development and standards-setting bodies for the Internet.

5.In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples? a file linked on the py4e website

6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet? (NOTE: examples in the previous videos no longer work. I suspect this is because HTTP/1.0 is no longer supported by the webserver). I can retrieve the document using telnet. 
    
