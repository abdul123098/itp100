1. What function does Python use for accessing files? What arguments does it       need? What does it return?
   Open function. This function returns a file object, also called a handle, as    it is used to read or modify the file accordingly. We can specify the mode      while opening a file. In mode, we specify whether we want to read r , write w   or append a to the file.

2. What is a file handle? Explain how it relates to a file? Where is the data      stored?                                                                          It allows users to handle files i.e., to read and write files, along with       many other file handling options, to operate on files. The data is stored in file.

3. What is '\n'?   '\n' is a newline.

4. What does Dr. Chuck say is the most common way to treat a file when reading     it?  Common way to treat a file read line by line. 

5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead? Explain your answer.  We can use slicing in the reverse direction by cutting /n.  

6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'. Compare these three patterns, providing examples of each.    The first one is line.startswith and second is not line.startswith and it has a continue statement and last he showed is not @uct.ac.za. 

7. A new Python construct is introduced at the end of the second video to deal     with the situation when the program attempts to open a file that isn't there.   Describe this new construct and the two new keywords that pair to make it. 
 The try block lets you test a block of code for errors. The except block lets you handle the error. The finally block lets you execute code, regardless of the result of the try- and except blocks.
       

