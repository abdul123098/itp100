# Lesson 6 notes

1.  Dr. Chuck calls looping the 4th basic pattern of computer programming. What     are the other three patterns?        

    The other patterns are sequence , conditional, store and reuse.

2. Describe the program Chr. Chuck uses to introduce the while loop. What does     it do? 

   While loop is much like a if statement. The while loop is like a question       that leads to a true and false answer. The condition may be any expression,     and true is any non-zero value. The loop iterates while the condition is true.
 
3. What is another word for looping introduced in the video? 

   iterate

4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop        called? What is wrong with it?

   It is called an infinite loop. In this kind of loop, we are not changing and    it will run true forever until you hit the button.

5. What Python statement (a new keyword for us, by the way) can be used to exit    a loop?

   We can use break statements to exit a loop.

6. What is the other loop control statement introduced in the first video? What    does it do?  

   The other loop is called continue. It basically ends the current iteration      and jumps to the top of the loop and starts the next iteration.

7. Dr. Chuck ends the first video by saying that a while is what type of loop?     Why are they called that? What is the type of loop he says will be discussed    next? 

   While loops are called indefinite loops because they keep going until a         logical condition becomes false. He will discuss a definite loop next.

8. Which new Python loop is introduced in the second video? How does it work?      
   The new Python loops are For loops. They have explicit iteration variables      that change each time through a loop. These iteration variables move through    the sequence of sets.

9. The third video introduces what Dr. Chuck calls loop idioms. What does he       mean by that term? Which examples of loop idioms does he introduce in this      and the fourth video?

   Loop idioms are patterns in how we construct loops. He showed an example of a   set of six numbers. It is going to run and print some numbers out and it will   run its loop six times and away. This loop only prints stuff out. Another       example is what is the largest number? He showed in this example that           computers work differently from our brains. Another example he showed is        finding the largest value.


     
  


   


