# Lesson 5
 `
1. How can we use Markdown and git to create nice looking documents under revision control?
Markdown is a light-weight markup language for quickly making nice documents

2.  How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python.
 A function is a block of organized, reusable code that is used to perform a single, related action. ... As you already know, Python gives you many built-in functions like print(),  We call the function by using the function name, parenthesis

3.  Dr. Chuck shows that nothing is output when you define a function, what he calls the store phase. What does he call the process that makes the function run? He uses two words for this, and it is really important to understand this idea and learn the words for it.   He used the word invoke and call.
4. Provide some examples of built-in function in Python.
print(), list(), float()


