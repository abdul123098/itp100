1. Dr. Chuck mentions the architecture that runs our network. What is the name of this architecture?  Transport Control Protocol

2. Take a close look at the slide labelled Stack Connections. Which layer does Dr. Chuck say we will be looking at, assuming that one end of this layer can make a phone call to the corresponding layer on the other end? What are the names of the two lower level layers that we will be abstracting away?  Lower layer. Transport that will assume to make a call to another computer. 

3. We will be assuming that there is a ____________ that goes from point A to point B. There is a ______________ running at each end of the connection. Fill in the blanks.    Pipe & socket

4. Define Internet socket as discussed in the video. an endpoint of a bidirectional inter-process communication flow across an Internet Protocol-based computer network, such as the Internet.”

5.Define TCP port as discussed in the video.A port is an application-specific or process-specific software communications endpoint.

6. At which network layer do sockets exist? Application layer

7. Which network protocol is used by the World Wide Web? At which network layer does it operate?  Hyper Transfer Protocol, application layer
