1. In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

Algorithms are steps to solve a problem and data structures is a particular way of organizing data. Algorithm that we have been focusing on.

2. Describe in your own words what a collection is.  It is used to store values in a variable i.e x = 1



3. Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bears repeating. What is that point?

It is convenient to write friends instead of friend because it helps the reading to understand better and it doesn’t have any effect on python because python doesn’t know plurals and singles.  


4. How do we access individual items in a list?  We can access individual items by using an index specified in square brackets. 

5. What does mutable mean? Provide examples of mutable and immutable data types in Python. 

Another word for mutable changeable means we change items in a list. 
Mutable:
>>> x = [19, 67]
>>> x[1] = 4
>>> print(x)
[19, 4]
Immutable:
>>> x = ‘apple’
>>> x[1] = c
>>> print(x) 
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'c' is not defined


6. Describe several list operations presented in the lecture.

Plus + operator is used to add two lists and we can also use slicing by getting a specific item from a list. In operator check for items in the list. 

7. Describe several list methods presented in the lecture.

Count looks for certain values in the list. Extend adds things to the end of the list. Inserts allow the list to expand in the middle. Pop pulls things off the top. And remove removes an item in the middle.

8. Describe several useful functions that take lists as arguments presented in the lecture.

 len function tells how many items in the list. Max looks for the largest, min looks for the smallest, and sum function adds all the items.

9. The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.

Split function breaks a string into parts and produces a list of strings.

10. What is a guardian pattern? Use at least one specific example in describing this important concept.

Guardian pattern is where we construct a logical expression with additional comparisons to take advantage of the short-circuit behavior.
fhand = open('mbox-short.txt')
count = 0
for line in fhand:
    words = line.split()
    # print('Debug:', words)
    if len(words) == 0 : continue
    if words[0] != 'From' : continue
    print(words[2])
 
