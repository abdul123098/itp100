def greet(lang):
    if lang == "french":
        print("Bonjour")
    elif lang == "english":
        print("Hello")
    elif lang == "spanish":
        print("Hola")

greet("french")
greet("english")


def greets():
    return "Good"

print(greets(), "morning")
print(greets(), "night")


def addthree(a,b,c):
    added = a + b + c 
    return added

i = addthree(4,5,6)
print(i)
