def sum_of_squares_of_digits(n):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    sum=0
    for num in str(int(n)):
        sum=sum+int(num)**2
    print(sum)

    
if __name__ == '__main__':
    import doctest
    doctest.testmod()    
